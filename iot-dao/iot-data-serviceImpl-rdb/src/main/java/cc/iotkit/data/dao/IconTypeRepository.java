package cc.iotkit.data.dao;

import cc.iotkit.data.model.TbIconType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author：tfd
 * @Date：2024/4/28 16:41
 */
public interface IconTypeRepository extends JpaRepository<TbIconType, Long> {
}
